#encoding=utf-8

import socket
import threading
from random import shuffle
import time


class server:
	def __init__(self):	
		self.cards, self.weights = self.cards()

		self.data_recv = []  
		self.data_send = []

		self.playerNumber = self.getNumber()   #玩家人数

		self.conn_list = []	  #已建立链接的sock列表

		self.players = {}  #玩家信息字典 {}

		HOST='127.0.0.1'
		PORT=30000
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print 'Socket has been created!'
		self.sock.bind((HOST, PORT))
		self.sock.listen(5)
		print 'Socket is listening'


	def main(self):
		#服务器开收发线程
		threading.Thread(target = self.SendData).start()
		while True:
			if len(self.conn_list) < self.playerNumber:
				#玩家没有全部上线
				conn, addr = self.sock.accept()
				print 'Connected with ' + addr[0] + ':' + str(addr[1])
				self.conn_list.append(conn) # 将刚刚连接上的玩家

				threading.Thread(target = self.RecvData, args = (conn, )).start()
				while True:
					if self.data_recv:
						print "data_recv=",self.data_recv
						head, nickname = self.data_recv.pop(0).split("$") #收线程从客户端接收到玩家目前的操作类型以及昵称

						#初始化玩家信息，包括玩家昵称以及玩家初始得分
						if head == "LogIn": 
							self.players[nickname] = {}   #名字为nickname的玩家，现在的得分是0
							self.players[nickname]["score"] = 0   #名字为nickname的玩家，现在的得分是0
							self.players[nickname]["cards"] = []  #名字为nickname的玩家，现在没有卡牌
							self.data_send.append("Online$" + ",".join(self.players.keys()))  #将目前所有在线玩家的昵称发送给客户端用于显示
							break
					else:
						time.sleep(0.5)
			else:
				#玩家已全部上线并初始化完毕，开始接收客户端发来的请求, 发送卡牌
				print "all players are online!" + ",".join(self.players.keys()) 
				self.data_send.append("Ready$" + ",".join(self.players.keys()))
				while True:
					self.turn()

					#判断游戏是否结束
					scores = []
					flag = False
					for name in self.players.keys():
						scores.append(self.players[name]["score"])

					scores.sort()
					#如果分数超过21且第二名小于20，或者第一名超过21且与第二名得分差2分以上，则游戏结束，广播winner的昵称以及所有卡牌
					if scores[-1]>=21 and scores[-2]<20 or (scores[-1]>=21 and scores[-1]-scores[-2] >1):
						for name in self.players.keys():
							if self.players[name]["score"] == scores[-1]:
								self.data_send.append("Game$" + name + ","+self.players[name]["cards"])
						return 
					else:
						self.data_send.append("Turn$ ")

	def getNumber(self):
		# get the number of players
		while True:
			try:
				n = int(raw_input("Please input the number of players:"))
				while  n > 4 or n < 2:
					print "Wrong input! Please keep the number between 2 and 4!"
					n = int(raw_input("Please input the number of players:"))

				return n
			except:
				print "Wrong input! Please input number!"


	def RecvData(self, connect):
		print "RecvData is used" 
		while True:
			# try:
			self.data_recv.append(connect.recv(1024))   #接收客户端发来的消息
			print "In RecvData:"+self.data_recv[0]
			# except:
			# 	print "fail recevie"
			# 	break

	def SendData(self):
		while True:
			try:
				if self.data_send:
					print "In Send data:", self.data_send
					command = self.data_send.pop(0)
					for connect in self.conn_list:
						temp = connect.send(command)

					# print temp
			except:
				print "fail send"
				


	def turn(self):
		cards_temp = {}
		winner_turn = None #每轮的牌面最大者
		for name in self.players.keys():
			self.data_send.append("Assign$" + name +"," + str(len(self.cards)))
			while True:
				if self.data_recv:
					selectedCards = self.cards.pop(int(self.data_recv.pop(0)))  #玩家选中的卡牌
					cards_temp[name] = selectedCards
					self.data_send.append("CardBroad$" + name + ":" + str(selectedCards))
					self.players[name]["cards"].append(selectedCards)   #将该玩家抽到的卡牌放入玩家的数据字典中

					if winner_turn:
						if self.weights[cards_temp[winner_turn]] > self.weights[selectedCards]:
							winner_turn = name
					else:
						winner_turn = name
					break

					#如果当前玩家选中罚牌，且其分数大于0，则得分减一
					if self.weights[selectedCards] == 0 and self.players[name]["score"] > 0:
							self.players[name]["score"] -= 1
				else:
					time.sleep(0.5)

		#本轮牌面最大的玩家，得分加2
		self.players[winner_turn]["score"] += 2


	def cards(self):
		cards = []
		weights = {}  
		w = 1
		for i in range(2, 11):
			for j in ["Club","Diamond", "Heart","Spade"]:
				cards.append((i, j))
				weights[(i,j)] = w
				w += 1

		for i in ["Jack", "Queen", "King", "Ace"]:
			for j in ["Club","Diamond", "Heart","Spade"]:
				cards.append((i, j))
				weights[(i,j)] = w
				w += 1

		for k in range(4):
			cards.append((0, "Penalty"))
		weights[(0, "Penalty")] = 0

		shuffle(cards)

		return cards, weights



	


if __name__ == "__main__":
	s = server()
	s.main()