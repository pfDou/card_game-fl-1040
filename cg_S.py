#encoding=utf-8

import socket
import threading
import sys


def RecvData(connect):
	global data_recv
	while True:
		print "saaaaaaaaaaaaaaa"
		try:
			data_recv = connect.recv(1024)   #接收客户端发来的消息
			print "RecvData:"+len(data_recv)
		except:
			pass

def SendData(connect):
	global data_send
	while data_send:
		try:
			connect.send(data_send)
			data_send = None
		except:
			pass

# get the number of players
n = int(raw_input("Please input the number of players:"))
while  n > 4 or n < 2:
	print "Wrong input! Please keep the number between 2 and 4!"
	n = int(raw_input("Please input the number of players:"))


HOST='219.245.80.190'
PORT=50007
data_recv = None
data_send = None	

players = {}

#建立socket连接
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket has been created!'
s.bind((HOST, PORT))
s.listen(5)
print 'Socket is listening'

conn_list = []  #已连接socket列表
while True:	
	if len(conn_list) < n:
		#玩家没有全部上线
		conn, addr = s.accept()
		print 'Connected with ' + addr[0] + ':' + str(addr[1])
		conn_list.append(conn) # 将刚刚连接上的玩家
		#为每位玩家开收发线程
		threading.Thread(target = RecvData, args = (conn, )).start()
		threading.Thread(target = SendData, args = (conn, )).start()

		print data_recv
		head, nickname = data_recv.split("$") #收线程从客户端接收到玩家目前的操作类型以及昵称

		#初始化玩家信息，包括玩家昵称以及玩家初始得分
		if head == "LogIn": 
			players[nickname] = 0   #名字为nickname的玩家，现在的得分是0
			data_send = "Online$" + ",".join(players.keys())   #将目前所有在线玩家的昵称发送给客户端用于显示

	else:
		#玩家已全部上线并初始化完毕，开始接收客户端发来的请求发送卡牌
		print "all players are online!" + ",".join(players.keys()) 
		break





